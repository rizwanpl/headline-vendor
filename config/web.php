
<?php
if (session_status() == PHP_SESSION_NONE)
{
    session_start();
}

$hosturl = $_SESSION['hosturlbypl'] = 'localhost';
$dbusername = $_SESSION['dnameurlbypl'] = 'root';
$dbpassword = $_SESSION['dpasurlbypl'] = '';
if ((isset($_SERVER['SERVER_NAME']) && ($_SERVER['SERVER_NAME'] == 'hairdressingjobs.co.uk' || $_SERVER['SERVER_NAME'] == 'www.hairdressingjobs.co.uk'))) {
    $dbpassword = $_SESSION['dpasurlbypl'] = 'Dedmere#123';
    $hosturl = $_SESSION['hosturlbypl'] = 'localhost';
    $_SESSION['cert_path'] = 'file:///var/www/HeadlineYii/certs-live/';
    $_SESSION['consumer_key'] = "JBTGVKGSZ8WJ6ZRVGOK9KLUZPDRGWZ";
    $_SESSION['consumer_secret'] = "AA9LJAZIQ5XGKUBDHJPGST14LCPVB4";
    $_SESSION['app'] = "Headline-live";
}else{
    $_SESSION['cert_path'] = "file://certs-local/";
    $_SESSION['consumer_key'] = "NJSCK1R9UPYNIBKFDIO1Z6WPXXAMLX";
    $_SESSION['consumer_secret'] = "U9FF8GDFZZTHQVPSB1P3SQNGUSMWPS";
    $_SESSION['app'] = "HeadlineYii2";
}

$params  = require(__DIR__ . '/params.php');
$dbnames = (empty($_SESSION['siteData']['database_name']) || !isset($_SESSION['siteData']['database_name'])) ? 'HeadlineYii' : $_SESSION['siteData']['database_name'];
$dbusername = (empty($_SESSION['siteData']['database_user']) || !isset($_SESSION['siteData']['database_user'])) ? 'root' : $_SESSION['siteData']['database_user'];
$dbpassword = (empty($_SESSION['siteData']['database_password']) || !isset($_SESSION['siteData']['database_password'])) ? $dbpassword : $_SESSION['siteData']['database_password'];

if(!empty($headline)){
    $dbnames = 'HeadlineYii';
    $dbusername = 'root';
    $dbpassword = '';
}
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'purelogics123456',
        ],
        
        'MyGlobalComp'=>[
            'class'=>'app\components\MyGlobalComp'
         ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
        ],
        'utility' => [ 
            'class' => 'app\components\utility',
        ],
        'DocutilXML' => [ 
            'class' => 'app\components\DocutilXML',
        ],
        'DocxConversion' => [ 
            'class' => 'app\components\DocxConversion',
        ],
         'Filetotext' => [ 
            'class' => 'app\components\Filetotext',
        ],
        'CI_Email' => [ 
            'class' => 'app\components\CI_Email',
        ],
        'CustomPagination' => [ 
            'class' => 'app\components\CustomPagination',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
           // 'viewPath' =>'@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => '109.203.98.170',
                'username' => 'admin@astonselectadmin.com',
                'password' => 'admin@123',
                'port' => '587',
                'encryption' => 'tls',
            ],
            
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'authManager' => [
                           'class' => 'yii\rbac\DbManager',
                           'defaultRoles' => ['guest'],
        ],
        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host='.$hosturl.';port=3306;dbname='.$dbnames,
            'username' => $dbusername,
            'password' => $dbpassword,
        ],
        'response' => [
            'formatters' => [
                'pdf' => [
                    'class' => 'robregonm\pdf\PdfResponseFormatter',
                ],
            ]
        ],
        'db' => require(__DIR__ . '/db.php'),
        'dbGlobal' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host='.$hosturl.';port=3306;dbname=globalDb',
            'username' => $dbusername,
            'password' => $dbpassword,
            'charset' => 'utf8',
        ],
        'mail' => [
            'class' => 'yashop\ses\Mailer',
            'access_key' => $params['amazonusername'],
            'secret_key' => $params['amazonpassword'],
            'host' => $params['amazonhost'] // not required
        ],
    ],
    
    'params' => $params,
];

if (YII_ENV_DEV) {
     $config['modules'] = [  'gridview' => [
            'class' => '\kartik\grid\Module', // see settings on http://demos.krajee.com/grid#module
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module', // see settings on http://demos.krajee.com/datecontrol#module
        ],
        // If you use tree table
        'treemanager' => [
            'class' => '\kartik\tree\Module', // see settings on http://demos.krajee.com/tree-manager#module
        ]
    ];
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';
    $config['bootstrap'][] = 'MyGlobalComp';
}

return $config;
