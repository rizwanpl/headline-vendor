<?php

Yii::setAlias('@tests', dirname(__DIR__) . '/tests');

$params = require(__DIR__ . '/params.php');
$db = require(__DIR__ . '/db.php');
$dbGlobal = require(__DIR__ . '/dbGlobal.php');

return [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii'],
    'controllerNamespace' => 'app\commands',
    'modules' => [
        'gii' => 'yii\gii\Module',
    ],
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'dbGlobal' => $dbGlobal,
        'mail' => [
            'class' => 'yashop\ses\Mailer',
            'access_key' => $params['amazonusername'],
            'secret_key' => $params['amazonpassword'],
            'host' => $params['amazonhost'] // not required
        ],
    ],
    'params' => $params,
];
