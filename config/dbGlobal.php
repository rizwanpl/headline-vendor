<?php
$globaldbname = 'globalDb';
//$FILEPATH = realpath(dirname(__FILE__));
//if ($FILEPATH == '/var/www/HeadlineYii/config') {
//    $globaldbname   = 'live_globalDb';
//}
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;port=3306;dbname='.$globaldbname,
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
];